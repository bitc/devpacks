module Devpacks.Tarball where

import Conduit
import Control.Exception
import Control.Monad (when)
import Data.Bits ((.&.), (.|.))
import Data.List (intercalate)
import Data.Maybe (fromMaybe)
import System.Directory (createDirectoryIfMissing, renamePath)
import System.Directory (listDirectory)
import System.FilePath ((</>))
import System.IO.Temp (withTempDirectory)
import System.PosixCompat.Files (createSymbolicLink)
import System.PosixCompat.Files (getFileStatus, fileMode, setFileMode, ownerExecuteMode, groupExecuteMode, otherExecuteMode)
import qualified Codec.Archive.Tar as Tar
import qualified Codec.Archive.Tar.Check as Tar
import qualified Codec.Archive.Tar.Entry as Tar
import qualified Codec.Compression.GZip as GZip
import qualified Codec.Compression.Lzma as Lzma
import qualified Data.ByteString.Lazy.Char8 as L
import qualified System.FilePath as F

-- | Extract a compressed or non-compressed tar file to a destination directory
extractTarball
    :: FilePath -- ^ File to extract. Must end with ".tar" or ".tar.gz" or ".tar.xz"
    -> FilePath -- ^ Destination directory
    -> IO ()
extractTarball tarball dir = do
    (t, _) <- tarballType tarball
    let decompress = case t of
            TarballUncompressed -> id
            TarballGz -> GZip.decompress
            TarballXz -> Lzma.decompress
    contents <- L.readFile tarball
    let entries = Tar.read (decompress contents)
    unpackTar dir entries

-- | Similar to 'extractTarball', but additionally:
--
--   * Verifies that the tarball extracts to a a single directory with a
--     name matching that of the tarball name
--
--   * Acts atomically. If the operation is canceled or the process exits
--     or crashes then no files will be written to the destination directory
--     (there may be a directory called "tmp" containing files)
--
--   * Makes sure everything is safely written with fsync (TODO)
--
safeExtractTarball
    :: FilePath -- ^ File to extract. Must end with ".tar" or ".tar.gz" or ".tar.xz"
    -> FilePath -- ^ Destination directory
    -> IO ()
safeExtractTarball tarball dir = do
    (_, expectedDirName) <- tarballType tarball
    createDirectoryIfMissing True (dir </> "tmp")
    withTempDirectory (dir </> "tmp") ("tmp-" ++ expectedDirName) $ \tmpDir -> do
        extractTarball tarball tmpDir
        dirContents <- listDirectory tmpDir
        when (dirContents /= [expectedDirName]) $ do
            -- TODO Error messages should limit the number of dirContents elems shown
            fail $ "Invalid tarball, does not extract cleanly. Contents: " ++ intercalate ", " dirContents
        renamePath (tmpDir </> expectedDirName) (dir </> expectedDirName)

tarballType :: FilePath -> IO (TarballType, FilePath)
tarballType filepath
    | F.takeExtension filepath == ".tar" = pure (TarballUncompressed, F.takeBaseName filepath)
    | F.takeExtension filepath == ".gz" && F.takeExtension (F.dropExtension filepath) == ".tar" = pure (TarballGz, F.takeBaseName (F.dropExtension filepath))
    | F.takeExtension filepath == ".xz" && F.takeExtension (F.dropExtension filepath) == ".tar" = pure (TarballXz, F.takeBaseName (F.dropExtension filepath))
    | otherwise = fail $ "Can't detect tarball type: " ++ filepath

data TarballType
    = TarballUncompressed
    | TarballGz
    | TarballXz

unpackTar :: FilePath -> Tar.Entries Tar.FormatError -> IO ()
unpackTar dir x =
    (loop Nothing Nothing . (checkEntries checkEntrySecurity)) x
  where
    loop :: (Maybe FilePath) -> (Maybe FilePath) -> Tar.Entries (Either Tar.FormatError Tar.FileNameError) -> IO ()
    loop _ _ Tar.Done = return ()
    loop _ _ (Tar.Fail e) = either throwIO throwIO e
    loop pathOverride linkOverride (Tar.Next e es) = case Tar.entryContent e of
        Tar.Directory -> loop Nothing Nothing es
        Tar.NormalFile lbs _ -> do
            let fp = dir </> fromMaybe (Tar.entryPath e) pathOverride
            saveFile fp lbs (Tar.entryPermissions e)
            loop Nothing Nothing es
        Tar.SymbolicLink target -> do
            let fp = dir </> fromMaybe (Tar.entryPath e) pathOverride
            let link = fromMaybe (Tar.fromLinkTarget target) linkOverride
            saveSymbolicLink fp link
            loop Nothing Nothing es
        Tar.OtherEntryType 'x' pax _ -> do
            case parsePaxExtended pax of
                Left err -> fail err
                Right overrides -> do
                    let pathOverride' = lookup (L.pack "path") overrides
                        linkOverride' = lookup (L.pack "linkpath") overrides
                    loop (fmap L.unpack pathOverride') (fmap L.unpack linkOverride') es
        _ -> do
            loop Nothing Nothing es

saveFile :: FilePath -> L.ByteString -> Tar.Permissions -> IO ()
saveFile fp lbs perms = do
    createDirectoryIfMissing True $ F.takeDirectory fp
    runConduitRes $ mapM_ yield (L.toChunks lbs) .| sinkFile fp
    when ((perms .&. (ownerExecuteMode .|. groupExecuteMode .|. otherExecuteMode)) /= 0 ) $ do
        stat <- getFileStatus fp
        setFileMode fp (fileMode stat .|. (ownerExecuteMode .|. groupExecuteMode .|. otherExecuteMode))

saveSymbolicLink :: FilePath -> FilePath -> IO ()
saveSymbolicLink fp target = do
    createDirectoryIfMissing True $ F.takeDirectory fp
    createSymbolicLink target fp

checkEntries :: (Tar.Entry -> Maybe e') -> Tar.Entries e -> Tar.Entries (Either e e')
checkEntries checkEntry =
    Tar.mapEntries (\entry -> maybe (Right entry) Left (checkEntry entry))

checkEntrySecurity :: Tar.Entry -> Maybe Tar.FileNameError
checkEntrySecurity _ = Nothing

parsePaxExtended :: L.ByteString -> Either String [(L.ByteString, L.ByteString)]
parsePaxExtended = next []
    where
    next :: [(L.ByteString, L.ByteString)] -> L.ByteString -> Either String [(L.ByteString, L.ByteString)]
    next accum str =
        let (lenStr, _) = L.break (==' ') str
        in case L.readInt lenStr of
            Nothing -> Left "Invalid PAX"
            Just (len, _) ->
                let (seg, rest) = L.splitAt (fromIntegral len) str
                    seg' = L.drop 1 (L.dropWhile (/=' ') seg)
                    seg'' = L.take (L.length seg' - 1) seg'
                    (key, val) = L.break (=='=') seg''
                    newPair = (key, L.drop 1 val)
                in if L.null rest then Right (newPair:accum)
                    else next (newPair:accum) rest
