{-# LANGUAGE OverloadedStrings #-}

module Devpacks.Command.Shell where

import Data.HashMap.Strict (HashMap, foldMapWithKey)
import Data.List (intercalate)
import Data.Maybe (catMaybes, fromMaybe)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Versions (Versioning)
import Devpacks.Command.Shell.EnvVars (PathVarElem (..), PathVarValue, parsePathVarValue)
import Devpacks.DevpacksConfig
import Devpacks.DevpacksManifest.Types (DevpacksManifest (shellConfig), ShellConfig (shellEnvVars), ShellEnvVar (..), availableGroups, filterNeededPackages, getGroupPackages)
import Devpacks.ExecProcess (execProcess)
import Devpacks.Operations (getBinPaths)
import Devpacks.OsArch
import Devpacks.PackageManagement (installPackages, packageDir)
import Devpacks.Types
import System.Environment (getEnv, getEnvironment, lookupEnv, setEnv)
import System.Exit (die)
import System.FilePath (getSearchPath, searchPathSeparator)
import System.IO (stdout)
import System.IO.Temp (writeSystemTempFile, writeTempFile)

-- | On success does not return
doShell :: OsArch -> DevpacksConfig -> [GroupName] -> DevpacksManifest -> IO ()
doShell osArch config grps manifest = do
    case getGroupPackages osArch grps manifest of
        Left notFound ->
            die $
                unlines
                    [ "Groups not found in devpacks.toml:"
                    , intercalate ", " (map (T.unpack . unGroupName) notFound)
                    , "Available groups:"
                    , "base, " ++ intercalate ", " (map (T.unpack . unGroupName) (availableGroups manifest))
                    ]
        Right pkgs -> do
            installPackages osArch config (filterNeededPackages pkgs) stdout
            let projectName = "devpacks" -- TODO
            binPaths <- getBinPaths osArch config (filterNeededPackages pkgs)
            currentSearchPath <- getSearchPath
            let newSearchPath = binPaths ++ currentSearchPath
                newPath = intercalate [searchPathSeparator] newSearchPath
            env <- getEnvironment
            manifestEnvVars <- case getShellEnvVars osArch config pkgs (shellEnvVars (shellConfig manifest)) of
                Left err -> die err
                Right manifestEnvVars -> pure manifestEnvVars
            if currentOs == "windows"
                then startShellCmd projectName newPath env manifestEnvVars
                else startShellBash projectName newPath env manifestEnvVars

startShellBash :: String -> FilePath -> [(String, String)] -> [(String, String)] -> IO a
startShellBash projectName newPath env manifestEnvVars = do
    let env2 =
            ( setEnvVar "PATH" newPath
                . setEnvVar "DEVPACKS_SHELL" "1"
                -- . setEnvVar "DEVPACKS_SHELL_MANIFEST_FILE" "TODO"
                -- . setEnvVar "DEVPACKS_SHELL_MANIFEST_FILE_HASH" "TODO"
            )
                env
    let shellEnv = env2 ++ manifestEnvVars
    launchBash projectName shellEnv

startShellCmd :: String -> FilePath -> [(String, String)] -> [(String, String)] -> IO a
startShellCmd projectName newPath env manifestEnvVars = do
    -- We use "setEnv" on the current process rather than passing in a new
    -- environment dictionary to "execProcess" because on windows there are
    -- weird glitches with the PATH
    setEnv "PROMPT" $ case lookup "PROMPT" env of
        Nothing -> "(" ++ projectName ++ ") $P$G"
        Just val -> "(" ++ projectName ++ ") " ++ val
    setEnv "PATH" newPath
    setEnv "DEVPACKS_SHELL" "1"
    -- setEnv "DEVPACKS_SHELL_MANIFEST_FILE" "TODO"
    -- setEnv "DEVPACKS_SHELL_MANIFEST_FILE_HASH" "TODO"
    mapM_ (uncurry setEnv) manifestEnvVars
    launchCmd

-- | If any variables are empty, then returns 'Nothing'
expandPathVarValue :: Text -> OsArch -> DevpacksConfig -> [(PackageName, Maybe Versioning)] -> PathVarValue -> Either String (Maybe Text)
expandPathVarValue varName osArch config pkgs val =
    case mapM expandElem val of
        Left err -> Left err
        Right mbExpanded -> case sequence mbExpanded of
            Nothing -> Right Nothing
            Just frags -> Right $ Just $ T.concat frags
  where
    expandElem :: PathVarElem -> Either String (Maybe Text)
    expandElem (PathVarElem_TextFragment frag) = Right (Just frag)
    expandElem (PathVarElem_VarInterpolation var) =
        let pkgName = PackageName var
         in case lookup pkgName pkgs of
                Nothing -> Left $ "Error in environment variable \"" <> T.unpack varName <> "\": Package not found: \"" <> T.unpack var <> "\""
                Just Nothing -> Right Nothing
                Just (Just v) -> Right $ Just $ T.pack $ packageDir osArch config pkgName v

setEnvVar :: String -> String -> [(String, String)] -> [(String, String)]
setEnvVar name value vars =
    (name, value) : filter (\(var, _) -> var /= name) vars

getShellEnvVars :: OsArch -> DevpacksConfig -> [(PackageName, Maybe Versioning)] -> HashMap Text ShellEnvVar -> Either String [(String, String)]
getShellEnvVars osArch config pkgs = sequence . foldMapWithKey processVar
  where
    processVar :: Text -> ShellEnvVar -> [Either String (String, String)]
    processVar varName varValue = case expandVar varName varValue of
        Left err -> [Left err]
        Right v -> [Right (T.unpack varName, T.unpack v)]

    expandVar :: Text -> ShellEnvVar -> Either String Text
    expandVar _ (ShellEnvVar_Val val) = Right val
    expandVar varName (ShellEnvVar_Path pathVal) =
        let parsed = map parsePathVarValue pathVal
            expanded = map (expandPathVarValue varName osArch config pkgs) parsed
         in case sequence expanded of
                Left err -> Left err
                Right values -> Right $ T.intercalate (T.pack [searchPathSeparator]) (catMaybes values)

-- Implementation inspired by nix shell
--
-- See: <https://github.com/NixOS/nix/blob/00f99fdfe6ae9d16d938b4276323bbf28c5872ce/src/nix-build/nix-build.cc#L430>
launchBash :: String -> [(String, String)] -> IO a
launchBash projectName env = do
    rcfile <-
        writeSystemTempFile "bashrc" $
            unlines
                [ "[ -e ~/.bashrc ] && source ~/.bashrc;"
                , "PS1=\"\\[\\033[01;33m\\][" <> projectName <> "] \\[\\033[0m\\]$PS1\""
                ]
    execProcess "bash" ["--rcfile", rcfile] (Just env)

launchCmd :: IO a
launchCmd = do
    execProcess "cmd" [] Nothing
