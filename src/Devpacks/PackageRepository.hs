{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeApplications #-}

module Devpacks.PackageRepository where

--import Data.Conduit
--import Control.Monad.Trans.Resource
--import qualified Data.Conduit.Binary as CB
import Conduit
import qualified Data.Conduit.Combinators as C
import System.IO (stdout)
import Network.HTTP.Simple
import Network.HTTP.Conduit
import Data.Versions (Versioning, versioning)
import Devpacks.Types
import Control.Exception (tryJust)
import Devpacks.DevpacksConfig
import Data.Conduit.List (mapMaybe)
import Network.HTTP.Types.Status (statusCode)
import Data.Text (Text)
import qualified Data.Text as T
import System.IO (hPutStrLn, stderr)
import Devpacks.DevpacksConfig.Types (DevpacksConfigMirror(..), DevpacksConfig(..))
import qualified Data.List.NonEmpty as NonEmpty

listPackages_ :: IO ()
listPackages_ = do
    --runResourceT $ CB.sourceFile "blah.txt" $$ CB.sinkFile "x.txt"
    runConduitRes $ sourceFileBS "testmirror/index.txt" .| sinkHandle stdout
    req <- parseUrlThrow "http://localhost:8000/index.txt"
    runConduitRes $ httpSource req getResponseBody .| sinkHandle stdout

-- TODO This is temporary:
mainMirror :: DevpacksConfig -> Text
mainMirror DevpacksConfig{mirrors} = url (NonEmpty.head mirrors)

packageVersionsUrl :: DevpacksConfig -> PackageName -> Text
packageVersionsUrl config pkg = (mainMirror config) <> (unPackageName pkg) <> ".txt"

listPackageVersions :: DevpacksConfig -> PackageName -> IO ()
listPackageVersions config pkg = do
    req <- parseUrlThrow (T.unpack (packageVersionsUrl config pkg))
    mbList <- tryJust guardStatus404 $
        runConduitRes $ httpSource req getResponseBody .| C.decodeUtf8 .| C.linesUnbounded .| C.unlines .| C.encodeUtf8 .| C.stdout
    case mbList of
        Left _ -> hPutStrLn stderr "Package does not exist"
        Right _ -> pure ()

latestPackageVersion :: DevpacksConfig -> PackageName -> IO (Maybe Versioning)
latestPackageVersion config pkg = do
    req <- parseUrlThrow (T.unpack (packageVersionsUrl config pkg))
    mbList <- tryJust guardStatus404 $
        runConduitRes $ httpSource req getResponseBody .| C.decodeUtf8 .| C.linesUnbounded .| mapMaybe getVersion .| C.maximum
    case mbList of
        Left _ -> pure Nothing
        Right v -> pure v

getVersion :: Text -> Maybe Versioning
getVersion v = case versioning v of
    Left _ -> Nothing
    Right x -> Just x

guardStatus404 :: HttpException -> Maybe ()
guardStatus404 (HttpExceptionRequest _ (StatusCodeException rsp _)) | statusCode (responseStatus rsp) == 404 = Just ()
guardStatus404 _ = Nothing
