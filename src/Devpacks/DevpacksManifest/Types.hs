{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Devpacks.DevpacksManifest.Types where

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as H
import Data.Hashable (Hashable)
import Data.Maybe (maybeToList)
import Data.Text (Text)
import Data.Versions (Versioning)
import Devpacks.OsArch (OsArch, osArchOs)
import Devpacks.Types (GroupName (..), PackageName)
import GHC.Generics (Generic)

data DevpacksManifest = DevpacksManifest
    { packageGroups :: HashMap GroupName PackageGroup
    , shellConfig :: ShellConfig
    }
    deriving (Show)

data PackageGroup = PackageGroup
    { packages :: HashMap PackageName PackageVersionSpec
    }
    deriving (Show)

data OverrideKey
    = OverrideOs Text
    | OverrideOsArch OsArch
    deriving (Show, Eq, Ord, Generic)

instance Hashable OverrideKey

data PackageVersionSpec = PackageVersionSpec
    { defaultVersion :: Maybe Versioning
    , overrides :: HashMap OverrideKey (Maybe Versioning)
    }
    deriving (Show)

data ShellConfig = ShellConfig
    { shellEnvVars :: HashMap Text ShellEnvVar
    }
    deriving (Show)

data ShellEnvVar
    = ShellEnvVar_Val Text
    | ShellEnvVar_Path [Text]
    deriving (Show)

basePackages :: DevpacksManifest -> HashMap PackageName PackageVersionSpec
basePackages DevpacksManifest{packageGroups} =
    case H.lookup (GroupName "base") packageGroups of
        Just PackageGroup{packages} -> packages
        Nothing -> H.empty

availableGroups :: DevpacksManifest -> [GroupName]
availableGroups DevpacksManifest{packageGroups} = H.keys packageGroups

-- | A Left result indicates that the requested groups don't exist in the manifest
getGroupPackages :: OsArch -> [GroupName] -> DevpacksManifest -> Either [GroupName] [(PackageName, Maybe Versioning)]
getGroupPackages osArch [] m = Right (allPackageVersions m osArch)
getGroupPackages osArch groups DevpacksManifest{packageGroups} =
    case partitionWith
        ( \g -> case H.lookup g packageGroups of
            Nothing -> Left g
            Just group -> Right group
        )
        groups of
        ([], packageGroups) -> Right (concatMap (\packageGroup -> packageGroupVersions packageGroup osArch) packageGroups)
        (missing, _) -> Left missing

filterNeededPackages :: [(PackageName, Maybe Versioning)] -> [(PackageName, Versioning)]
filterNeededPackages =
    concatMap
        ( \(p, y) -> case y of
            Nothing -> []
            Just v -> [(p, v)]
        )

getPackageVersion :: OsArch -> PackageVersionSpec -> Maybe Versioning
getPackageVersion osArch PackageVersionSpec{defaultVersion, overrides} =
    case H.lookup (OverrideOsArch osArch) overrides of
        Just (Just v) -> Just v
        Just Nothing -> Nothing -- Explicitly removed
        Nothing -> case H.lookup (OverrideOs (osArchOs osArch)) overrides of
            Just (Just v) -> Just v
            Just Nothing -> Nothing -- Explicitly removed
            Nothing -> defaultVersion

packageGroupVersions :: PackageGroup -> OsArch -> [(PackageName, Maybe Versioning)]
packageGroupVersions PackageGroup{packages} osArch =
    H.foldMapWithKey
        (\n s -> [(n, getPackageVersion osArch s)])
        packages

allPackageVersions :: DevpacksManifest -> OsArch -> [(PackageName, Maybe Versioning)]
allPackageVersions DevpacksManifest{packageGroups} osArch =
    concatMap (\p -> packageGroupVersions p osArch) (H.elems packageGroups)

-- | Uses a function to determine which of two output lists an input element should join
partitionWith :: (a -> Either b c) -> [a] -> ([b], [c])
partitionWith _ [] = ([], [])
partitionWith f (x : xs) = case f x of
    Left b -> (b : bs, cs)
    Right c -> (bs, c : cs)
  where
    (bs, cs) = partitionWith f xs
