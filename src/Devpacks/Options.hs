{-# LANGUAGE DeriveGeneric #-}
{-# Language ApplicativeDo #-}

module Devpacks.Options where

import GHC.Generics
import Data.Monoid ((<>))
import Options.Applicative
import Data.Versions (Versioning, versioning, errorBundlePretty)
import System.Info (os, arch, compilerName, compilerVersion)
import Data.Version (showVersion)
import Data.Text as T
import Devpacks.OsArch (OsArch, prettyOsArch)
import Devpacks.Types
import qualified System.FilePath as F
import Paths_devpacks (version)

data PackageQuery
    = PackageQueryRootDir
    | PackageQueryBinDir
    deriving (Show)

data Options = Options
    { config :: FilePath
    , silent :: Bool
    , verbose :: Bool
    , noNetwork :: Bool
    , cmd :: Command
    }
    deriving (Generic, Show)

data PkgPathSpec
    = PkgPathRoot
    | PkgPathBinDir
    | PkgPathProgram ProgName
    deriving (Show)

data QuickRunVersion
    = QuickRunVersionSpecific Versioning
    | QuickRunVersionInstalled
    | QuickRunVersionLatest
    deriving (Show)

data Command
    = Install [GroupName]
    | Run PackageName (Maybe Versioning) ProgName [ProgArg]
    | PkgPath PackageName (Maybe Versioning) PkgPathSpec
    | Shell [GroupName]
    | PathEnvVar Bool [GroupName]
    | CheckUpdates
    | CheckManifest
    | Info
    | Init
    | List
    | ListVersions PackageName
    | Search Text
    | QuickInstallPkg PackageName (Maybe Versioning)
    | QuickRun PackageName QuickRunVersion ProgName [ProgArg]
    | QuickPkgPath PackageName QuickRunVersion PkgPathSpec
    | GarbageCollect
    deriving (Show)

parsePackageName :: ReadM PackageName
parsePackageName = eitherReader $ \s ->
    -- TODO Detect invalid package name
    Right (PackageName (T.pack s))

parseVersion :: ReadM Versioning
parseVersion = eitherReader $ \s ->
    case versioning (T.pack s) of
        Left e -> Left $ "Error parsing version: " ++ s ++ "\n" ++ errorBundlePretty e
        Right v -> Right v

parseProgName :: ReadM ProgName
parseProgName = eitherReader $ \s ->
    if F.takeFileName s == s && F.isValid s
        then Right s
        else Left $ "Invalid file: " ++ s


cmdInstall :: Parser Command
cmdInstall = do
    groups <- many (argument (GroupName <$> str) (metavar "GROUPS..."))
    pure (Install groups)

cmdRun :: Parser Command
cmdRun = do
    pkg <- argument parsePackageName (metavar "PACKAGE")
    v <- optional (option parseVersion
            ( short 't'
            <> metavar "VERSION"
            <> help "Specify a specific package version"))
    progName <- argument parseProgName (metavar "PROG")
    args <- many (argument str (metavar "ARGS..."))
    pure (Run pkg v progName args)

cmdPkgPath :: Parser Command
cmdPkgPath = do
    pkg <- argument parsePackageName (metavar "PACKAGE")
    v <- optional (option parseVersion
            ( short 't'
            <> metavar "VERSION"
            <> help "Specify a specific package version"))
    s <- ((option (PkgPathProgram <$> parseProgName)
            ( short 'p'
            <> metavar "PROG"
            <> help "Get the path of a specific program within the package"))
          <|> (flag PkgPathRoot PkgPathBinDir
            ( long "bindir"
            <> help "Get the path of the bin directory within the package")))
    pure (PkgPath pkg v s)

cmdShell :: Parser Command
cmdShell = do
    groups <- many (argument (GroupName <$> str) (metavar "GROUPS..."))
    pure (Shell groups)

cmdPathEnvVar :: Parser Command
cmdPathEnvVar = do
    inherit <- switch (long "inherit" <> short 'i' <> help "Use the existing PATH var and add the new entries to the beginning")
    groups <- many (argument (GroupName <$> str) (metavar "GROUPS..."))
    pure (PathEnvVar inherit groups)

cmdCheckUpdates :: Parser Command
cmdCheckUpdates = pure CheckUpdates

cmdCheckManifest :: Parser Command
cmdCheckManifest = pure CheckManifest

cmdInfo :: Parser Command
cmdInfo = pure Info

cmdInit :: Parser Command
cmdInit = pure Init

cmdList :: Parser Command
cmdList = pure List

cmdListVersions :: Parser Command
cmdListVersions = do
    pkg <- argument parsePackageName (metavar "PACKAGE")
    pure (ListVersions pkg)

cmdSearch :: Parser Command
cmdSearch = do
    pattern <- argument str (metavar "PATTERN")
    pure (Search pattern)

cmdQuickInstallPkg :: Parser Command
cmdQuickInstallPkg = do
    pkg <- argument parsePackageName (metavar "PACKAGE")
    v <- optional (option parseVersion
            ( short 't'
            <> metavar "VERSION"
            <> help "Specify a specific package version"))
    pure (QuickInstallPkg pkg v)

cmdQuickRun :: Parser Command
cmdQuickRun = do
    pkg <- argument parsePackageName (metavar "PACKAGE")
    v <- ((option (QuickRunVersionSpecific <$> parseVersion)
            ( short 't'
            <> metavar "VERSION"
            <> help "Specify a specific package version"))
          <|> (flag QuickRunVersionInstalled QuickRunVersionLatest
            ( long "latest"
            <> help "Use the latest version of the package that is available in the world-wide repository. If this is omitted then the latest installed version will be used")))
    progName <- argument parseProgName (metavar "PROG")
    args <- many (argument str (metavar "ARGS..."))
    pure (QuickRun pkg v progName args)

cmdQuickPkgPath :: Parser Command
cmdQuickPkgPath = do
    pkg <- argument parsePackageName (metavar "PACKAGE")
    v <- ((option (QuickRunVersionSpecific <$> parseVersion)
            ( short 't'
            <> metavar "VERSION"
            <> help "Specify a specific package version"))
          <|> (flag QuickRunVersionInstalled QuickRunVersionLatest
            ( long "latest"
            <> help "Use the latest version of the package that is available in the world-wide repository. If this is omitted then the latest installed version will be used")))
    s <- ((option (PkgPathProgram <$> parseProgName)
            ( short 'p'
            <> metavar "PROG"
            <> help "Get the path of a specific program within the package"))
          <|> (flag PkgPathRoot PkgPathBinDir
            ( long "bindir"
            <> help "Get the path of the bin directory within the package")))
    pure (QuickPkgPath pkg v s)

cmdGarbageCollect :: Parser Command
cmdGarbageCollect = pure GarbageCollect

versionFlag :: OsArch -> Parser (a -> a)
versionFlag osArch = infoOption ("devpacks version " ++ showVersion version ++ " " ++ (T.unpack (prettyOsArch osArch)) ++ " (" ++ compilerName ++ "-" ++ showVersion compilerVersion ++ ")")
  (  long "version"
  <> help "Print version information"
  <> hidden)

pinfo :: OsArch -> FilePath -> ParserInfo Options
pinfo osArch defaultConfigFile = info (parser osArch defaultConfigFile) (fullDesc <> footer "For help on a specific command, invoke it with COMMAND --help")

parser :: OsArch -> FilePath -> Parser Options
parser osArch defaultConfigFile = do
    x <- option str (long "config" <> metavar "FILE" <> value defaultConfigFile <> showDefault <> help "devpacks config.toml file" <> hidden)
    verboseFlag <- switch (short 'v' <> long "verbose" <> help "Print extra output to stdout and stderr")
    silentFlag <- switch (short 's' <> long "silent" <> help "Only print fatal messages to stderr")
    noNetworkFlag <- switch (long "no-network" <> help "Don't download any package files or package metadata. If a command would otherwise require this then it will fail" <> hidden)
    let s = hsubparser
                (  command "install" (info cmdInstall $
                    progDesc "Install all or some of the packages of the current project")
                <> command "run" (info cmdRun $
                    progDesc "Run a program from a package")
                <> command "pkg-path" (info cmdPkgPath $
                    progDesc "Get the path where a package is installed")
                <> command "shell" (info cmdShell $
                    progDesc "Enter a subshell with the programs of the packages the current project available")
                <> command "path-env-var" (info cmdPathEnvVar $
                    progDesc "Get a suitable PATH environment variable to enable using the packages of the current project")
                <> command "check-updates" (info cmdCheckUpdates $
                    progDesc "Check if there are newer versions available in the world-wide repository for the current project's declared packages")
                <> command "check-manifest" (info cmdCheckManifest $
                    progDesc "Check that the devpacks.toml file is valid")

                <> commandGroup "Project commands (require a devpacks.toml):"
                )
            <|> hsubparser
                (  command "info" (info cmdInfo $
                    progDesc "Print system info")
                <> command "init" (info cmdInit $
                    progDesc "Interactive wizard to create a devpacks.toml file for a new project")
                <> command "list" (info cmdList $
                    progDesc "List the packages that are available in the world-wide repository")
                <> command "list-versions" (info cmdListVersions $
                    progDesc "List the versions of a package that are available in the world-wide repository")
                <> command "search" (info cmdSearch $
                    progDesc "Search for a package in the world-wide repository")
                <> command "quick-install-pkg" (info cmdQuickInstallPkg $
                    progDesc "Install a single package")
                <> command "quick-run" (info cmdQuickRun $
                    progDesc "Run a program from a package")
                <> command "quick-pkg-path" (info cmdQuickPkgPath $
                    progDesc "Get the path where a package is installed")
                <> command "garbage-collect" (info cmdGarbageCollect $
                    progDesc "Delete unused packages to free up disk space")

                <> commandGroup "Non-project commands (ignores devpacks.toml):"
                <> hidden
                )
    command_ <- (helper <*> versionFlag osArch <*> s)
    pure $ Options
        { cmd = command_
        , config = x
        , silent = silentFlag
        , verbose = verboseFlag
        , noNetwork = noNetworkFlag
        }
