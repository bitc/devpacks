{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Devpacks.OsArch where

import Data.Hashable (Hashable)
import Data.Text (Text)
import qualified Data.Text as T
import GHC.Generics (Generic)
import System.Info (arch, os)

-- | Operating System and CPU Architecture
data OsArch = OsArch
    { osArchOs :: Text
    , osArchArch :: Text
    }
    deriving (Show, Eq, Ord, Generic)

prettyOsArch :: OsArch -> Text
prettyOsArch OsArch{osArchOs, osArchArch} = osArchOs <> "-" <> osArchArch

instance Hashable OsArch

currentOsArch :: IO OsArch
currentOsArch =
    pure $
        OsArch
            { osArchOs = currentOs
            , osArchArch = T.pack arch
            }

currentOs :: Text
currentOs
    | os == "mingw32" = "windows"
    | otherwise = T.pack os
