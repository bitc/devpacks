{-# LANGUAGE OverloadedStrings #-}

module Devpacks.DevpacksConfig.Types (
    DevpacksConfig (..),
    DevpacksConfigMirror (..),
    defaultDevpacksConfig,
) where

import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.Text (Text)
import System.FilePath ((</>))

data DevpacksConfig = DevpacksConfig
    { packagesDir :: FilePath
    , mirrors :: NonEmpty DevpacksConfigMirror
    }
    deriving (Show)

data DevpacksConfigMirror = DevpacksConfigMirror
    { url :: Text
    }
    deriving (Show)

defaultDevpacksConfig :: DevpacksConfig
defaultDevpacksConfig =
    DevpacksConfig
        { mirrors =
            DevpacksConfigMirror
                { url = "https://mirror1.devpacks.org/"
                }
                :| []
        , packagesDir = "." </> "packages"
        }
