{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DataKinds #-}

module Devpacks.Command.Install where

import Data.Versions (Versioning)
import Devpacks.Types
import Devpacks.DevpacksConfig (DevpacksConfig)
import Devpacks.DevpacksManifest
import Devpacks.OsArch
import Data.List (foldl', nub)
import System.Exit (die)
import Data.List (intercalate)
import qualified Data.Text as T
import qualified Data.HashMap.Strict as H
import Devpacks.PackageManagement
import System.IO (stdout)
import Devpacks.DevpacksManifest.Types (filterNeededPackages, availableGroups, getGroupPackages, DevpacksManifest)

doInstall :: OsArch -> DevpacksConfig -> [GroupName] -> DevpacksManifest -> IO ()
doInstall osArch config grps manifest =
    case getGroupPackages osArch grps manifest of
        Left notFound -> die $ unlines
            [ "Groups not found in devpacks.toml:"
            , intercalate ", " (map (T.unpack . unGroupName) notFound)
            , "Available groups:"
            , "base, " ++ intercalate ", " (map (T.unpack . unGroupName) (availableGroups manifest))
            ]
        Right pkgs -> do
            installPackages osArch config (filterNeededPackages pkgs) stdout
