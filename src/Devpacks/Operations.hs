module Devpacks.Operations where

import Control.Monad (forM)
import Data.Versions (Versioning)
import Devpacks.DevpacksConfig.Types (DevpacksConfig)
import Devpacks.OsArch (OsArch)
import Devpacks.PackageManagement (packageDir)
import Devpacks.Types (PackageName)
import System.Directory (doesDirectoryExist)
import System.FilePath ((</>))
import System.IO (stdout)

getBinPaths :: OsArch -> DevpacksConfig -> [(PackageName, Versioning)] -> IO [FilePath]
getBinPaths osArch config packages = do
    dirs <- forM packages $ \(packageName, packageVersion) -> do
        let dir = packageDir osArch config packageName packageVersion </> "bin"
        exists <- doesDirectoryExist dir
        pure $ if exists then [dir] else []
    pure $ concat dirs
