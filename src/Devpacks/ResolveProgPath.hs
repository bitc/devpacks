{-# LANGUAGE OverloadedStrings #-}

module Devpacks.ResolveProgPath where

import Devpacks.OsArch (currentOs)
import System.Directory (doesFileExist)

resolveProgPath :: FilePath -> IO FilePath
resolveProgPath
    | currentOs == "windows" = resolveProgPathWindows
    | otherwise = pure

-- | Searches for an executable with an approprate file extension
resolveProgPathWindows :: FilePath -> IO FilePath
resolveProgPathWindows filePath = do
    c1 <- doesFileExist filePath
    if c1 then pure filePath
        else do
            c2 <- doesFileExist (filePath ++ ".exe")
            if c2 then pure (filePath ++ ".exe")
                else do
                    c3 <- doesFileExist (filePath ++ ".cmd")
                    if c3 then pure (filePath ++ ".cmd")
                        else do
                            c4 <- doesFileExist (filePath ++ ".bat")
                            if c4 then pure (filePath ++ ".bat")
                                else pure filePath
