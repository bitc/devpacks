module Devpacks.ConfigManagement where

import Devpacks.DevpacksConfig
import qualified Data.ByteString as B
import System.Directory (getAppUserDataDirectory)
import System.FilePath ((</>), takeDirectory)
import Control.Exception (tryJust)
import Control.Monad (guard)
import System.IO.Error (isDoesNotExistError)
import System.Directory (createDirectoryIfMissing)
import qualified Data.Text.Encoding as T
import qualified Toml
import Devpacks.DevpacksConfig.Toml (devpacksConfigCodec, defaultDevpacksConfigFile)

defaultConfigFileLocation :: IO FilePath
defaultConfigFileLocation = do
    appDir <- getAppUserDataDirectory "devpacks"
    pure (appDir </> "config.toml")

-- | Reads the config file, or create and read it if it doesn't exist and trying to read the default config file
readConfigFile :: FilePath -> IO B.ByteString
readConfigFile configFile = do
    defaultConfigFile <- defaultConfigFileLocation
    if (configFile /= defaultConfigFile)
        then B.readFile configFile
        else do
            r <- tryJust (guard . isDoesNotExistError) $ B.readFile configFile
            case r of
                Left _ -> do
                    let newConfigFileContents = defaultDevpacksConfigFile
                    createDirectoryIfMissing False $ takeDirectory configFile
                    B.writeFile configFile newConfigFileContents
                    pure newConfigFileContents
                Right contents -> pure contents

loadConfigFile :: FilePath -> IO DevpacksConfig
loadConfigFile configFile = do
    contents <- readConfigFile configFile
    case T.decodeUtf8' contents of
        Left err -> fail (show err)
        Right contentsText -> case Toml.decode devpacksConfigCodec contentsText of
            Left err -> fail $ "Error loading " ++ configFile ++ "\n" ++ show err
            Right val -> do
                normalizePackagesDir configFile val
