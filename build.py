import os
import os.path
import platform
import subprocess
import time


def main():
    CABAL = get_pkg_prog("haskell-cabal-install", "cabal")
    GHC = get_pkg_prog("ghc", "ghc")

    C_INCLUDE_PATH = []
    LIBRARY_PATH = []

    register_c_library("zlib", C_INCLUDE_PATH, LIBRARY_PATH)
    register_c_library("xz", C_INCLUDE_PATH, LIBRARY_PATH)

    set_env("C_INCLUDE_PATH", os.pathsep.join(C_INCLUDE_PATH))
    set_env("LIBRARY_PATH", os.pathsep.join(LIBRARY_PATH))

    run(GHC, ["--version"])
    run(CABAL, ["--version"])

    if cabal_index_older_than(hours=48):
        run(CABAL, ["v2-update"])

    run(CABAL, ["v2-build", "--with-ghc=" + GHC])


def get_cabal_dir():
    if platform.system().lower() == "windows":
        return os.path.join(os.getenv("APPDATA"), "cabal")
    else:
        return os.path.join(os.path.expanduser("~"), ".cabal")


def cabal_index_older_than(hours):
    CABAL_DIR = get_cabal_dir()
    CABAL_INDEX = os.path.join(CABAL_DIR, "packages", "hackage.haskell.org", "01-index.tar")
    if not os.path.isfile(CABAL_INDEX):
        return True
    return time.time() - os.path.getmtime(CABAL_INDEX) > (hours * 60 * 60)


def register_c_library(pkg_name, C_INCLUDE_PATH, LIBRARY_PATH):
    lib_path = get_pkg_path(pkg_name)
    if lib_path:
        C_INCLUDE_PATH.append(os.path.join(lib_path, "include"))
        LIBRARY_PATH.append(os.path.join(lib_path, "lib"))


def get_pkg_path(pkg_name):
    return subprocess.run(
        ["devpacks", "pkg-path", pkg_name], encoding="utf-8", check=True, stdout=subprocess.PIPE
    ).stdout.strip()


def get_pkg_prog(pkg_name, prog):
    return subprocess.run(
        ["devpacks", "pkg-path", pkg_name, "-p", prog],
        encoding="utf-8",
        check=True,
        stdout=subprocess.PIPE,
    ).stdout.strip()


def run(cmd, args):
    print(f"++ {cmd} {str(args)}")
    subprocess.run([cmd] + args, check=True)


def set_env(var, value):
    print(f"++ export {var}={value}")
    os.environ[var] = value


if __name__ == "__main__":
    main()
