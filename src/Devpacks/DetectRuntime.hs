module Devpacks.DetectRuntime where

import GHC.Environment (getFullArgs)
import System.FilePath (takeFileName)

detectIsGhci :: IO Bool
detectIsGhci = do
    fullArgs <- getFullArgs
    case fullArgs of
        [] -> pure False
        (prog : _) -> pure $ (takeFileName prog) == "ghc"
