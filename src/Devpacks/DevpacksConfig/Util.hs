{-# LANGUAGE OverloadedStrings #-}

module Devpacks.DevpacksConfig.Util where

import Data.Text (Text)
import qualified Data.Text as T
import Network.URI (parseURI, uriFragment, uriQuery, uriScheme)

validateMirrorUrl :: Text -> Either Text Text
validateMirrorUrl u = case parseURI (T.unpack u) of
    Nothing -> Left "Mirror URL is malformed"
    Just uri
        | not (elem (uriScheme uri) ["http:", "https:"]) ->
            Left $
                "Mirror URL has unsupported scheme: " <> T.pack (show (uriScheme uri)) <> ". Only \"http:\" and \"https:\" are allowed"
        | not (null (uriQuery uri)) ->
            Left $
                "Mirror URL cannot contain a query string. Found: " <> T.pack (show (uriQuery uri))
        | not (null (uriFragment uri)) ->
            Left $
                "Mirror URL cannot contain a fragment string. Found: " <> T.pack (show (uriFragment uri))
        | otherwise ->
            let withSlash =
                    if T.last u == '/'
                        then u
                        else u <> "/"
             in Right withSlash
