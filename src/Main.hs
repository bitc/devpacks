{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Devpacks.Options
import Options.Applicative (execParser)
import Devpacks.ConfigManagement
import Devpacks.OsArch (currentOsArch)
import Devpacks.Command.Install (doInstall)
import Devpacks.Command.PathEnvVar (doPathEnvVar)
import Devpacks.Command.QuickRun (doQuickRun)
import Devpacks.Command.ListVersions (doListVersions)
import Devpacks.Command.PkgPath (doPkgPath)
import Devpacks.Command.QuickPkgPath (doQuickPkgPath)
import Devpacks.Command.Shell (doShell)
import Devpacks.DevpacksManifest

main :: IO ()
main = do
    defaultConfigFile <- defaultConfigFileLocation 
    osArch <- currentOsArch
    opts <- execParser (pinfo osArch defaultConfigFile)
    let Options{config, cmd} = opts
    cfg <- loadConfigFile config
    case cmd of
        Install groups -> loadRequiredDevpacksManifest >>= doInstall osArch cfg groups
        PathEnvVar inherit groups -> loadRequiredDevpacksManifest >>= doPathEnvVar osArch cfg inherit groups
        PkgPath pkg v spec -> loadRequiredDevpacksManifest >>= doPkgPath osArch cfg pkg v spec
        -- CheckManifest -> loadRequiredDevpacksManifest >>= doCheckYaml
        ListVersions pkg -> doListVersions osArch cfg pkg
        QuickRun pkg v prog args -> doQuickRun osArch cfg pkg v prog args
        QuickPkgPath pkg v spec -> doQuickPkgPath osArch cfg pkg v spec
        Shell groups -> loadRequiredDevpacksManifest >>= doShell osArch cfg groups
        todo -> putStrLn "TODO" >> print todo
