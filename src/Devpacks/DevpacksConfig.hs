{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Devpacks.DevpacksConfig
    ( DevpacksConfig
    , normalizePackagesDir
    ) where

import System.Directory (canonicalizePath)
import System.FilePath
import Devpacks.DevpacksConfig.Types (DevpacksConfig(..))

normalizePackagesDir :: FilePath -> DevpacksConfig -> IO DevpacksConfig
normalizePackagesDir configFile config@DevpacksConfig{packagesDir} = do
    normalizedPackagesDir <- canonicalizePath $ (takeDirectory configFile) </> packagesDir
    pure $ config { packagesDir = normalizedPackagesDir }
