{-# LANGUAGE OverloadedStrings #-}

module Devpacks.Command.QuickPkgPath where

import Devpacks.DevpacksConfig
import Devpacks.Types
import Devpacks.Options (QuickRunVersion(..))
import Devpacks.Options (PkgPathSpec(..))
import Devpacks.OsArch
import Devpacks.ResolveProgPath (resolveProgPath)
import Data.Versions (Versioning)
import Devpacks.PackageManagement
import System.IO (stderr)
import System.FilePath ((</>))
import Devpacks.PackageRepository
import Devpacks.OsArch (currentOs)
import System.Exit (die)

doQuickPkgPath :: OsArch -> DevpacksConfig -> PackageName -> QuickRunVersion -> PkgPathSpec -> IO ()
doQuickPkgPath osArch config pkg (QuickRunVersionSpecific v) spec = do
    go osArch config pkg v spec
doQuickPkgPath osArch config pkg QuickRunVersionInstalled spec = do
    mbVersion <- latestInstalledPackage osArch config pkg
    case mbVersion of
        Just v -> go osArch config pkg v spec
        Nothing -> doQuickPkgPath osArch config pkg QuickRunVersionLatest spec
doQuickPkgPath osArch config pkg QuickRunVersionLatest spec = do
    latestVersion <- latestPackageVersion config pkg
    case latestVersion of
        Nothing -> die "Package does not exist"
        Just v -> doQuickPkgPath osArch config pkg (QuickRunVersionSpecific v) spec

go :: OsArch -> DevpacksConfig -> PackageName -> Versioning -> PkgPathSpec -> IO ()
go osArch config pkg version spec = do
    installPackages osArch config [(pkg, version)] stderr
    dir <- case spec of
        PkgPathRoot -> pure $ packageDir osArch config pkg version
        PkgPathBinDir -> pure $ packageDir osArch config pkg version </> "bin"
        PkgPathProgram prog -> do
            -- TODO Check that the file exists and is executable
            let filePath = packageDir osArch config pkg version </> "bin" </> prog
            resolved <- resolveProgPath filePath
            pure resolved
    putStr dir
