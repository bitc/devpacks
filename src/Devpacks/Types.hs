{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

module Devpacks.Types where

import Data.Hashable (Hashable)
import Data.Text (Text)

newtype PackageName = PackageName {unPackageName :: Text}
    deriving (Show, Eq, Ord, Hashable)

newtype GroupName = GroupName {unGroupName :: Text}
    deriving (Show, Eq, Ord, Hashable)

baseGroup :: GroupName
baseGroup = GroupName "base"

type ProgName = String
type ProgArg = String
