module Devpacks.Command.PkgPath where

import Data.Maybe (catMaybes)
import Data.Versions (Versioning, prettyV)
import Devpacks.Types
import Devpacks.DevpacksConfig (DevpacksConfig)
import Devpacks.DevpacksManifest
import Devpacks.OsArch
import Devpacks.ResolveProgPath (resolveProgPath)
import System.FilePath ((</>))
import Data.List (nub)
import System.Exit (die)
import qualified Data.Text as T
import Devpacks.PackageManagement
import Devpacks.Options (PkgPathSpec(..))
import Data.List (sort)
import System.IO (stderr)
import Devpacks.DevpacksManifest.Types (allPackageVersions, DevpacksManifest)

doPkgPath :: OsArch -> DevpacksConfig -> PackageName -> Maybe Versioning -> PkgPathSpec -> DevpacksManifest -> IO ()
doPkgPath osArch config pkg mbVersion spec manifest =
    let pkgs = allPackageVersions manifest osArch
        matches = nub (filter ((pkg ==) . fst) pkgs)
     in case matches of
            [] -> die $ "Package " ++ pkgName ++ " not found in devpacks.toml. Maybe add it?"
            _ ->
                let versions = nub (sort (catMaybes (map snd matches)))
                 in case mbVersion of
                        Just v -> case any (== v) versions of
                            False ->
                                die $
                                    unlines
                                        [ "Requested version is not listed devpacks.toml:"
                                        , pkgName ++ "-" ++ T.unpack (prettyV v)
                                        ]
                            True -> go osArch config pkg v spec
                        Nothing -> case versions of
                            [] ->
                                -- Package is listed but disabled for this
                                -- os/arch, so we return an empty result
                                pure ()
                            [v] -> go osArch config pkg v spec
                            _ ->
                                die $
                                    unlines
                                        ( ["Error: Multiple versions of " ++ pkgName ++ " found:"]
                                            ++ (map (T.unpack . prettyV) versions)
                                        )
  where
    pkgName = T.unpack (unPackageName pkg)

go :: OsArch -> DevpacksConfig -> PackageName -> Versioning -> PkgPathSpec -> IO ()
go osArch config pkg version spec = do
    installPackages osArch config [(pkg, version)] stderr
    dir <- case spec of
        PkgPathRoot -> pure $ packageDir osArch config pkg version
        PkgPathBinDir -> pure $ packageDir osArch config pkg version </> "bin"
        PkgPathProgram prog -> do
            -- TODO Check that the file exists and is executable
            let filePath = packageDir osArch config pkg version </> "bin" </> prog
            resolved <- resolveProgPath filePath
            pure resolved
    putStr dir
