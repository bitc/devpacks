module Devpacks.Command.ListVersions where

import Devpacks.Types
import Devpacks.DevpacksConfig
import Devpacks.OsArch
import Devpacks.PackageRepository (listPackageVersions)

doListVersions :: OsArch -> DevpacksConfig -> PackageName -> IO ()
doListVersions _osArch config pkg = do
    listPackageVersions config pkg
