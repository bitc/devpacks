{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DataKinds #-}

module Devpacks.Command.PathEnvVar where

import Data.Versions (Versioning)
import Devpacks.Types
import Devpacks.DevpacksConfig (DevpacksConfig)
import Devpacks.OsArch
import Data.List (foldl', nub)
import System.Exit (die)
import Data.List (intercalate)
import qualified Data.Text as T
import qualified Data.HashMap.Strict as H
import Devpacks.PackageManagement
import System.IO (stderr)
import Devpacks.DevpacksManifest.Types (allPackageVersions, DevpacksManifest)

doPathEnvVar :: OsArch -> DevpacksConfig -> Bool -> [GroupName] -> DevpacksManifest -> IO ()
doPathEnvVar osArch config _inherit grps manifest = undefined
    -- case getPackages grps manifest of
    --     Left notFound -> die $ unlines
    --         [ "Groups not found in devpacks.toml:"
    --         , intercalate ", " (map (T.unpack . unGroupName) notFound)
    --         , "Available groups:"
    --         , "base, " ++ intercalate ", " (map (T.unpack . unGroupName) (H.keys (manifest ^. field @"additionalPackageGroups")))
    --         ]
    --     Right pkgs -> do
    --         installPackages osArch config pkgs stderr
    --         -- TODO ...

-- | A Left result indicates that the requested groups don't exist in the manifest
getPackages :: OsArch -> [GroupName] -> DevpacksManifest -> Either [GroupName] [(PackageName, Versioning)]
getPackages osArch [] manifest = undefined -- Right (allPackageVersions manifest osArch)
getPackages osArch grps manifest = undefined
    -- let (notFound, pkgs) = (foldl' f ([], []) (nub grps)) :: ([GroupName], [(PackageName, Versioning)])
    -- in if null notFound then Right (nub ((manifest ^. field @"basePackages") ++ pkgs))
    --     else Left (reverse notFound)
    -- where
    --     f :: ([GroupName], [(PackageName, Versioning)]) -> GroupName -> ([GroupName], [(PackageName, Versioning)])
    --     f (notFound, pkgs) g = case lookupGroupPackages g manifest of
    --         Nothing -> (g:notFound, pkgs)
    --         Just ps -> (notFound, pkgs ++ ps)

