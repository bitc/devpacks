{-# LANGUAGE OverloadedStrings #-}

module Devpacks.PackageIndex where

import Conduit
import System.IO (stdout)
import Network.HTTP.Simple
import Network.HTTP.Conduit

listPackages :: IO ()
listPackages = do
    --runResourceT $ CB.sourceFile "blah.txt" $$ CB.sinkFile "x.txt"
    runConduitRes $ sourceFileBS "testmirror/index.txt" .| sinkHandle stdout
    req <- parseUrlThrow "http://localhost:8000/index.txt"
    runConduitRes $ httpSource req getResponseBody .| sinkHandle stdout
