{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Devpacks.PackageManagement where

import Data.Text (Text)
import Data.Versions (Versioning, versioning, prettyV)
import Devpacks.Types (PackageName, unPackageName)
import Devpacks.DevpacksConfig (DevpacksConfig)
import System.IO.Temp (withSystemTempDirectory)
import System.Directory (doesDirectoryExist)
import Conduit
import qualified Data.Conduit.Combinators as C
import Data.Conduit.List (mapMaybe)
import Network.HTTP.Simple
import Network.HTTP.Conduit
import qualified Data.Text as T
import Data.List (nub)
import Control.Monad (forM_, filterM, unless)
import System.FilePath ((</>))
import qualified System.FilePath as F
import Devpacks.Tarball (safeExtractTarball)
import Devpacks.OsArch
import System.IO (Handle, hPutStrLn)
import Devpacks.DevpacksConfig.Types (DevpacksConfigMirror(..), DevpacksConfig(..))
import qualified Data.List.NonEmpty as NonEmpty

mainMirror :: DevpacksConfig -> Text
mainMirror DevpacksConfig{mirrors} = url (NonEmpty.head mirrors)

installPackages :: OsArch -> DevpacksConfig -> [(PackageName, Versioning)] -> Handle -> IO ()
installPackages osArch config@DevpacksConfig{packagesDir} packages outH = do
    -- TODO lock file for each package

    uninstalled <- filterM ((pure . not =<<) . isPackageInstalled osArch config) pkgs

    unless (null uninstalled) $ do
        withSystemTempDirectory "devpacks" $ \tempDir -> do
            -- Download all files:
            forM_ uninstalled $ \(packageName, version) -> do
                let file = packageFile osArch packageName version
                    url = packageUrl (mainMirror config) packageName file
                hPutStrLn outH $ "Downloading " ++ file ++ "..."
                downloadFile url (tempDir </> file)

            forM_ uninstalled $ \(packageName, version) -> do
                let file = packageFile osArch packageName version
                hPutStrLn outH $ "Extracting " ++ file ++ "..."
                safeExtractTarball (tempDir </> file) (packagesDir </> T.unpack (unPackageName packageName))
            
    where
        pkgs = nub packages

latestInstalledPackage :: OsArch -> DevpacksConfig -> PackageName -> IO (Maybe Versioning)
latestInstalledPackage osArch DevpacksConfig{packagesDir} pkg = do
    let dir = packagesDir </> T.unpack (unPackageName pkg)
    pkgDirExists <- doesDirectoryExist dir
    if pkgDirExists then
        runConduitRes $ sourceDirectory dir .| C.map F.takeFileName .| mapMaybe (dirPackageVersion osArch pkg) .| C.maximum
        else pure Nothing

dirPackageVersion :: OsArch -> PackageName -> FilePath -> Maybe Versioning
dirPackageVersion osArch pkg dir =
    if dirLen < pkgNameLen + osArchLen + 3 -- 2 dash chars and at least 1 char for version
        then Nothing
        else
            let (prefix, rest) = T.splitAt pkgNameLen dirName
                (mid, suffix) = T.splitAt (dirLen - pkgNameLen - osArchLen) rest
            in if (prefix /= unPackageName pkg) || (suffix /= prettyOsArch osArch)
                then Nothing
                else if (T.head mid /= '-') || (T.last mid /= '-')
                    then Nothing
                    else case versioning (T.drop 1 (T.dropEnd 1 mid)) of
                        Left _ -> Nothing
                        Right v -> Just v
    where
        dirName = T.pack dir
        pkgNameLen = T.length (unPackageName pkg)
        osArchLen = T.length (prettyOsArch osArch)
        dirLen = T.length dirName

-- | Must only be called when a lock is currently held for the package
isPackageInstalled :: OsArch -> DevpacksConfig -> (PackageName, Versioning) -> IO Bool
isPackageInstalled osArch config (pkg, version) = do
    doesDirectoryExist (packageDir osArch config pkg version)

packageSlug :: OsArch -> PackageName -> Versioning -> FilePath
packageSlug osArch packageName version = T.unpack (T.concat [unPackageName packageName, "-", prettyV version, "-", prettyOsArch osArch])

packageDir :: OsArch -> DevpacksConfig -> PackageName -> Versioning -> FilePath
packageDir osArch DevpacksConfig{packagesDir} packageName version =
    packagesDir </> T.unpack (unPackageName packageName) </> packageSlug osArch packageName version

packageFile :: OsArch -> PackageName -> Versioning -> FilePath
packageFile osArch packageName version = packageSlug osArch packageName version ++ ".tar.xz"

packageUrl
    :: Text -- ^ Mirror (URL)
    -> PackageName
    -> String -- ^ Filename
    -> Text
packageUrl mirror pkg file = mirror <> (unPackageName pkg) <> "/" <> T.pack file

downloadFile :: Text -> FilePath -> IO ()
downloadFile url dest = do
    req <- parseUrlThrow (T.unpack url)
    runConduitRes $ httpSource req getResponseBody .| sinkFileBS dest
