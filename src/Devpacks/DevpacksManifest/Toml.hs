{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Devpacks.DevpacksManifest.Toml where

import Control.Applicative ((<|>))
import Control.Arrow (left)
import Control.Category ((>>>))
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as H
import Data.Hashable (Hashable)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Versions (Versioning, errorBundlePretty, prettyV, versioning)
import Devpacks.DevpacksManifest.Types (ShellEnvVar(..), ShellConfig(..), DevpacksManifest (..), OverrideKey (OverrideOs, OverrideOsArch), PackageGroup (..), PackageVersionSpec (..))
import Devpacks.OsArch (OsArch (..), prettyOsArch)
import Devpacks.Types (GroupName (..), PackageName (..))
import GHC.Generics (Generic)
import Toml (Key, TomlCodec, (.=))
import qualified Toml
import Control.Exception (IOException, try)

-- | Throws an exception if there is an error
loadDevpacksManifestToml :: FilePath -> IO (Either String DevpacksManifest)
loadDevpacksManifestToml fileName = do
    r <- try $ Toml.decodeFile devpacksManifestCodec fileName
    case r of
        Left e -> pure (Left (show (e :: IOException)))
        Right v -> pure (Right v)

groupNameBiMap :: Toml.TomlBiMap Key GroupName
groupNameBiMap = Toml._KeyText >>> Toml.iso GroupName unGroupName

packageNameBiMap :: Toml.TomlBiMap Key PackageName
packageNameBiMap = Toml._KeyText >>> Toml.iso PackageName unPackageName

packageGroupCodec :: Key -> TomlCodec PackageGroup
packageGroupCodec key =
    PackageGroup
        <$> Toml.tableHashMap packageNameBiMap packageVersionSpecCodec key .= packages

devpacksManifestCodec :: TomlCodec DevpacksManifest
devpacksManifestCodec =
    DevpacksManifest
        <$> Toml.tableHashMap groupNameBiMap packageGroupCodec "packages" .= packageGroups
        <*> Toml.table shellConfigCodec3 "shell" .= shellConfig

shellConfigCodec3 :: TomlCodec ShellConfig
shellConfigCodec3 = ShellConfig
    <$> shellEnvVarsCodec "env" .= shellEnvVars

shellEnvVarsCodec :: Key -> TomlCodec (HashMap Text ShellEnvVar)
shellEnvVarsCodec key = Toml.tableHashMap Toml._KeyText shellEnvVarCodec key

shellEnvVarCodec :: Key -> TomlCodec ShellEnvVar
shellEnvVarCodec key =
    (Toml.dimatch matchVal ShellEnvVar_Val (Toml.text key))
        <|> (Toml.dimatch matchPath ShellEnvVar_Path (Toml.arrayOf Toml._Text key))
  where
    matchVal :: ShellEnvVar -> Maybe Text
    matchVal (ShellEnvVar_Val val) = Just val
    matchVal (ShellEnvVar_Path _) = Nothing

    matchPath :: ShellEnvVar -> Maybe [Text]
    matchPath (ShellEnvVar_Path path) = Just path
    matchPath (ShellEnvVar_Val _) = Nothing

versioningBiMap :: Toml.TomlBiMap Text Versioning
versioningBiMap =
    Toml.BiMap
        (left (Toml.ArbitraryError . T.pack . errorBundlePretty) . versioning)
        (Right . prettyV)

tomlFalse :: Toml.TomlBiMap Bool ()
tomlFalse =
    Toml.BiMap
        ( \b ->
            if b
                then Left (Toml.ArbitraryError "Cannot be set to true")
                else Right ()
        )
        (const (Right False))

packageVersionSpecCodec :: Key -> TomlCodec PackageVersionSpec
packageVersionSpecCodec key =
    ( Toml.dimatch
        matchString
        ( \x ->
            PackageVersionSpec
                { defaultVersion = Just x
                , overrides = H.empty
                }
        )
        ((Toml.match (Toml.invert versioningBiMap >>> Toml._Text)) key)
    )
        <|> ( Toml.dimap
                specTableFromPackageVersionSpec
                specTableToPackageVersionSpec
                ( Toml.tableHashMap
                    (Toml._KeyText >>> specKeyBiMap)
                    ( \key ->
                        Toml.match
                            ( ( Toml.BiMap
                                    ( \v -> case v of
                                        Just _ -> Left $ Toml.ArbitraryError "Invalid version"
                                        Nothing -> Right ()
                                    )
                                    (const (Right Nothing))
                              )
                                >>> Toml.invert tomlFalse
                                >>> Toml._Bool
                            )
                            key
                            <|> Toml.match (Toml._Just >>> Toml.invert versioningBiMap >>> Toml._Text) key
                    )
                    key
                )
            )
  where
    -- matchTable = _
    matchString :: PackageVersionSpec -> Maybe Versioning
    matchString PackageVersionSpec{defaultVersion = Nothing} = Nothing
    matchString PackageVersionSpec{defaultVersion = Just v, overrides}
        | H.null overrides = Just v
        | otherwise = Nothing

overrideKeyBiMap :: Toml.TomlBiMap Text OverrideKey
overrideKeyBiMap = Toml.BiMap from to
  where
    from :: Text -> Either Toml.TomlBiMapError OverrideKey
    from str = case T.splitOn "-" str of
        [os, arch] ->
            Right
                ( OverrideOsArch
                    ( OsArch
                        { osArchOs = os
                        , osArchArch = arch
                        }
                    )
                )
        [os] -> Right (OverrideOs os)
        _ -> Left (Toml.ArbitraryError $ "Invalid override: \"" <> str <> "\"")
    to :: OverrideKey -> Either a Text
    to (OverrideOs os) = Right os
    to (OverrideOsArch osArch) = Right (prettyOsArch osArch)

data SpecKey
    = DefaultVersionKey
    | OverrideKey OverrideKey
    deriving (Show, Eq, Ord, Generic)

instance Hashable SpecKey

specKeyBiMap :: Toml.TomlBiMap Text SpecKey
specKeyBiMap = Toml.BiMap from to
  where
    from :: Text -> Either Toml.TomlBiMapError SpecKey
    from str
        | str == "version" = Right DefaultVersionKey
        | otherwise = OverrideKey <$> Toml.forward overrideKeyBiMap str
    to :: SpecKey -> Either Toml.TomlBiMapError Text
    to DefaultVersionKey = Right "version"
    to (OverrideKey o) = Toml.backward overrideKeyBiMap o

type SpecTable = HashMap SpecKey (Maybe Versioning)

specTableFromPackageVersionSpec :: PackageVersionSpec -> SpecTable
specTableFromPackageVersionSpec PackageVersionSpec{defaultVersion, overrides} =
    maybe H.empty (H.singleton DefaultVersionKey . Just) defaultVersion
        `H.union` H.foldMapWithKey (H.singleton . OverrideKey) overrides

specTableToPackageVersionSpec :: SpecTable -> PackageVersionSpec
specTableToPackageVersionSpec s =
    PackageVersionSpec
        { defaultVersion = H.lookupDefault Nothing DefaultVersionKey s
        , overrides = H.foldMapWithKey f s
        }
  where
    f :: SpecKey -> Maybe Versioning -> HashMap OverrideKey (Maybe Versioning)
    f (OverrideKey o) v = H.singleton o v
    f _ _ = mempty
