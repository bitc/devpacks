module Devpacks.DevpacksManifest where

import Devpacks.DevpacksManifest.Types (DevpacksManifest)
import Devpacks.DevpacksManifest.Toml (loadDevpacksManifestToml)

-- | Looks for a @devpacks.yaml@ file in the current directory, or some parent directory.
loadRequiredDevpacksManifest :: IO DevpacksManifest
loadRequiredDevpacksManifest = do
    r <- loadDevpacksManifestToml "devpacks.toml"
    case r of
        Left errMsg -> fail $ "Error loading devpacks.toml:\n" ++ errMsg
        Right v -> pure v
