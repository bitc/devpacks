{-# LANGUAGE OverloadedStrings #-}

module Devpacks.DevpacksConfig.Toml where

import Devpacks.DevpacksConfig.Types (defaultDevpacksConfig, DevpacksConfig (..), DevpacksConfigMirror (..))
import Devpacks.DevpacksConfig.Util (validateMirrorUrl)
import Toml (TomlCodec, (.=))
import qualified Toml
import qualified Data.ByteString as B
import qualified Data.Text.Encoding as T

devpacksConfigMirrorCodec :: TomlCodec DevpacksConfigMirror
devpacksConfigMirrorCodec =
    DevpacksConfigMirror
        <$> Toml.validate validateMirrorUrl Toml._Text "url" .= url

devpacksConfigCodec :: TomlCodec DevpacksConfig
devpacksConfigCodec =
    DevpacksConfig
        <$> Toml.string "packages_dir" .= packagesDir
        <*> Toml.nonEmpty devpacksConfigMirrorCodec "mirror" .= mirrors

defaultDevpacksConfigFile :: B.ByteString
defaultDevpacksConfigFile =
    "# devpacks config\n\n"
    `B.append`
    T.encodeUtf8 (Toml.encode devpacksConfigCodec defaultDevpacksConfig)
