{-# LANGUAGE OverloadedStrings #-}

module Devpacks.Command.Shell.EnvVars where

import Data.Text (Text)
import qualified Data.Text as T
import Data.Void (Void)
import Devpacks.DevpacksManifest.Types (ShellEnvVar (..))
import System.FilePath (searchPathSeparator)
import Text.Megaparsec
import Text.Megaparsec.Char

evaluateShellEnvVarValue :: ShellEnvVar -> Text
evaluateShellEnvVarValue (ShellEnvVar_Val val) = val
evaluateShellEnvVarValue (ShellEnvVar_Path paths) =
    T.intercalate (T.pack [searchPathSeparator]) (filter undefined (map undefined paths))

type PathVarValue = [PathVarElem]

data PathVarElem
    = PathVarElem_TextFragment Text
    | PathVarElem_VarInterpolation Text
    deriving (Show)

type Parser = Parsec Void Text

pathVarValue :: Parser PathVarValue
pathVarValue = many (try varInterpolation <|> textFragment) <* eof

-- | "${VAR}" --> "VAR"
varInterpolation :: Parser PathVarElem
varInterpolation = do
    varName <- between (char '$' >> char '{') (char '}') identifier
    pure (PathVarElem_VarInterpolation varName)

textFragment :: Parser PathVarElem
textFragment = do
    cs <- some (anySingleBut '$')
    pure (PathVarElem_TextFragment (T.pack cs))

identifier :: Parser Text
identifier = do
    c <- letterChar <|> char '_'
    cs <- many (alphaNumChar <|> char '_')
    pure (T.pack (c : cs))

test = case runParser pathVarValue "" "${X}" of
    Right x -> print x
    Left y -> putStrLn (errorBundlePretty y)

parsePathVarValue :: Text -> PathVarValue
parsePathVarValue str = case runParser pathVarValue "" str of
    Right r -> r
    Left _ -> []
