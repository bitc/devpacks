module Devpacks.Command.QuickRun where

import Devpacks.Types
import Devpacks.Options (QuickRunVersion(..))
import Devpacks.DevpacksConfig
import Devpacks.OsArch
import Devpacks.PackageManagement
import Devpacks.PackageRepository
import Devpacks.ExecProcess
import Devpacks.ResolveProgPath (resolveProgPath)
import System.FilePath ((</>))
import System.IO (stderr)
import System.Exit (die)

-- | On success does not return
doQuickRun :: OsArch -> DevpacksConfig -> PackageName -> QuickRunVersion -> ProgName -> [ProgArg] -> IO a
doQuickRun osArch config pkg (QuickRunVersionSpecific v) progName args = do
    installPackages osArch config [(pkg, v)] stderr
    let progPath = packageDir osArch config pkg v </> "bin" </> progName
    resolveProgPath progPath >>= \prog -> execProcess prog args Nothing
doQuickRun osArch config pkg QuickRunVersionInstalled progName args = do
    mbVersion <- latestInstalledPackage osArch config pkg
    case mbVersion of
        Just v -> do
            let progPath = packageDir osArch config pkg v </> "bin" </> progName
            resolveProgPath progPath >>= \prog -> execProcess prog args Nothing
        Nothing -> doQuickRun osArch config pkg QuickRunVersionLatest progName args
doQuickRun osArch config pkg QuickRunVersionLatest progName args = do
    latestVersion <- latestPackageVersion config pkg
    case latestVersion of
        Nothing -> die "Package does not exist"
        Just v -> doQuickRun osArch config pkg (QuickRunVersionSpecific v) progName args
