{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedStrings #-}

module Devpacks.ExecProcess ( execProcess ) where

#ifndef mingw32_HOST_OS
import System.Posix.Process (executeFile)
#endif

import System.Process
import System.Exit
import Devpacks.OsArch (currentOs)
import Devpacks.DetectRuntime (detectIsGhci)

-- | On success does not return
execProcess :: FilePath -> [String] -> Maybe [(String, String)] -> IO a
execProcess cmd args env
    | currentOs == "windows" = execProcessPortable cmd args env
    | otherwise = do
        isGhci <- detectIsGhci
        if isGhci then execProcessPortable cmd args env
            else execProcessUnix cmd args env

execProcessPortable :: FilePath -> [String] -> Maybe [(String, String)] -> IO a
execProcessPortable cmd args env = do
    (_, _, _, p) <- createProcess (proc cmd args) { env = env }
    exitCode <- waitForProcess p
    exitWith exitCode

execProcessUnix :: FilePath -> [String] -> Maybe [(String, String)] -> IO a
execProcessUnix cmd args env =
#ifndef mingw32_HOST_OS
    executeFile cmd True args env
#else
    error "execProcessUnix not available on windows"
#endif
